package ee.bcs.valiit.project.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

    @GetMapping("/")
    public String home() {
        return "/static/index.html";
    }

    @GetMapping("/dataset")
    public String dataset() {
        return "/static/dataset.html";
    }

    @GetMapping("dataset/germany")
    public String germany() {return "/static/germany.html";}

}
