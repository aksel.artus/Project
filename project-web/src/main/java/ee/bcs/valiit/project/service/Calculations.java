package ee.bcs.valiit.project.service;

import ee.bcs.valiit.project.data.*;
import ee.bcs.valiit.project.data.model.Coordinates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Service
public class Calculations {

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private DataSetRepository dataSetRepository;

    @Autowired
    private CompanyDataRepository customerRepository;

    public List<Company> list() {
        return companyRepository.findAll();
    }

    public Company get(Long id) {
        return companyRepository.getOne(id);
    }

    public List<DataSet> listDataSets(Long companyId) {
        return dataSetRepository.findByCompanyId(companyId);
    }


    @Transactional
    public Coordinates calculate(Long dataSetId) {
        BigDecimal sum1 = new BigDecimal(0);
        BigDecimal sum2 = new BigDecimal(0);
        BigDecimal quantitySum = new BigDecimal(0);
        BigDecimal weightedAverageLongitude = new BigDecimal(0);
        BigDecimal weightedAverageLatitude = new BigDecimal(0);
        List<CompanyData> list = dataSetRepository.getOne(dataSetId).getCompanyData();
        for (CompanyData data : list) {
            BigDecimal result1 = data.getLongitude().multiply(BigDecimal.valueOf(data.getQuantity()));
            sum1 = sum1.add(result1);
            BigDecimal result2 = data.getLatitude().multiply(BigDecimal.valueOf(data.getQuantity()));
            sum2 = sum2.add(result2);
            BigDecimal quantity1 = BigDecimal.valueOf(data.getQuantity());
            quantitySum = quantitySum.add(quantity1);
            BigDecimal weightedAverageLongitude1 = sum1.divide(quantitySum, 2, RoundingMode.HALF_EVEN);
            weightedAverageLongitude = weightedAverageLongitude1;
            BigDecimal weightedAverageLatitude1 = sum2.divide(quantitySum, 2, RoundingMode.HALF_EVEN);
            weightedAverageLatitude = weightedAverageLatitude1;
        }

        System.out.println("Long*V sum: " + sum1);
        System.out.println("Lat*V sum: " + sum2);
        System.out.println("Quantity sum: " + quantitySum);
        System.out.println("Weighted average longitude: " + weightedAverageLongitude);
        System.out.println("Weighted average latitude: " + weightedAverageLatitude);
        Coordinates coordinates = new Coordinates();
        coordinates.setWeightedAverageLatitude(weightedAverageLatitude);
        coordinates.setWeightedAverageLongitude(weightedAverageLongitude);
        return coordinates;
    }
}






