package ee.bcs.valiit.project.controller;

import ee.bcs.valiit.project.data.Company;
import ee.bcs.valiit.project.data.model.Coordinates;
import ee.bcs.valiit.project.service.Calculations;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;

@RestController
@RequestMapping("/kotidData")
@Slf4j

public class CalculationController {

    @Autowired
    Calculations calculations;


    @GetMapping(value = "/coordinates/{dataSetId}", produces = "application/json")
    public Coordinates result(@PathVariable Long dataSetId) {

        return calculations.calculate(dataSetId);
    }

    @GetMapping(value = "/list", produces = "application/json")
    public List<Company> list() {
        List<Company> list = calculations.list();
        list.sort(new Comparator<Company>() {
            @Override
            public int compare(Company o1, Company o2) {
                if (o1.getId().compareTo(o2.getId()) > 0) {
                    return 1;
                } else if (o1.getId().compareTo(o2.getId()) < 0) {
                    return -1;
                }

                return 0;
            }
        });

        return calculations.list();

    }


    @GetMapping(value = "/get/{id}", produces = "application/json")
    public Company get(@PathVariable Long id) {
        return calculations.get(id);
    }

}





