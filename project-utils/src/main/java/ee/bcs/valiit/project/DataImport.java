package ee.bcs.valiit.project;

import ee.bcs.valiit.project.data.*;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import java.io.*;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

@Data
@Component
@Slf4j
public class DataImport {

    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private DataSetRepository dataSetRepository;
    @Autowired
    private CompanyDataRepository companyDataRepository;

    private void start(String[] args) {

        try {
            final File folder = new File(args[0]);
            for (final File fileEntry : folder.listFiles()) {
                if (fileEntry.getAbsolutePath().endsWith(".txt")) {
                    log.debug(fileEntry.getAbsolutePath());
                    Path path = Paths.get(fileEntry.getAbsolutePath());
                    List<String> fileLines = Files.readAllLines(path);
                    Long companyId = null;
                    Long dataSetId = null;
                    for (int i = 0; i < fileLines.size(); i++) {
//                        Company company = null;
//                        DataSet dataSet = null;
                        if (i == 0) { // first line
                            String[] companyInfo = fileLines.get(i).split(";");
                            Company company = companyRepository.findByName(companyInfo[0]);
                            if (company == null) {
                                company = new Company();
                                company.setName(companyInfo[0]);
                                company = companyRepository.save(company);
                            }
                            companyId = company.getId();
                            DataSet dataSet = new DataSet();
                            dataSet.setName(companyInfo[1]);
                            dataSet.setCompanyId(company.getId());
                            dataSet = dataSetRepository.save(dataSet);
                            dataSetId = dataSet.getId();
                        } else { // all other lines
                            String[] lineParts = fileLines.get(i).split(";");
                            CompanyData data = new CompanyData();
                            data.setCity(lineParts[0]);
                            data.setCountry(lineParts[1]);
                            data.setQuantity(Long.parseLong(lineParts[2]));
                            data.setLongitude(new BigDecimal(lineParts[3]));
                            data.setLatitude(new BigDecimal(lineParts[4]));
                            data.setDataSetId(dataSetId);
                            companyDataRepository.save(data);
                        }

                    }
                }
            }
/*
            List<String> fileLines = Files.readAllLines(path);
            for (String fileLine : fileLines) {
                String[] lineParts = fileLine.split(";");
                Company customerData = new Company();
                customerData.setCity(lineParts[0]);
                customerData.setCountry(lineParts[1]);
                //customerData.setQuantity(Long.parseLong(lineParts[2]));
                //customerData.setLon(new BigDecimal(lineParts[3]));
                //customerData.setLat(new BigDecimal(lineParts[4]));
                companyRepository.save(customerData);
            }
*/
        } catch (Exception e) {
            System.err.println("Error while reading the import file!");
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        if (args.length > 0) {
            ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
            DataImport dataImport = applicationContext.getBean(DataImport.class);
            //dataImport.start(args);
        } else {
            System.out.println("Import file path invalid!");
        }
    }


}




