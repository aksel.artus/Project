DROP TABLE IF EXISTS company CASCADE;

CREATE TABLE company (
	id BIGSERIAL PRIMARY KEY,
	name VARCHAR(64) NOT NULL,
	unique (name)

);

DROP TABLE IF EXISTS data_set CASCADE;

CREATE TABLE data_set (
	id BIGSERIAL PRIMARY KEY,
	name VARCHAR(64) NOT NULL,
	company_id BIGINT references company(id) NOT NULL

);

DROP TABLE IF EXISTS company_data CASCADE;

CREATE TABLE company_data (
	id BIGSERIAL PRIMARY KEY,
	city VARCHAR(32) NOT NULL,
	country VARCHAR(32) NOT NULL,
	quantity BIGINT,
	lon NUMERIC(19, 2),
	lat NUMERIC(19, 2),
	data_set_id BIGINT references data_set(id) NOT NULL
);
