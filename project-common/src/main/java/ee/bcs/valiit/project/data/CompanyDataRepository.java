package ee.bcs.valiit.project.data;

import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

@Transactional
public interface CompanyDataRepository extends JpaRepository<CompanyData, Long> {
}
