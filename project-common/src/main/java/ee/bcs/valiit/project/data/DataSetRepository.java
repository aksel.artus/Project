package ee.bcs.valiit.project.data;

import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface DataSetRepository extends JpaRepository<DataSet, Long> {

    List<DataSet> findByCompanyId(Long id);

}
