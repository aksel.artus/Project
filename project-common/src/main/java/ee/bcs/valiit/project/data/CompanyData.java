package ee.bcs.valiit.project.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.BigInteger;

@Entity
@Table(name = "company_data")
@Data
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class CompanyData extends BaseEntity {

    @Column(name = "city")
    private String city;

    @Column(name = "country")
    private String country;

    @Column(name = "quantity")
    private Long quantity;

    @Column(name = "lon")
    private BigDecimal longitude;

    @Column(name = "lat")
    private BigDecimal latitude;

    @Column(name = "data_set_id")
    private Long dataSetId;

}
