package ee.bcs.valiit.project.data.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Coordinates {
    private BigDecimal weightedAverageLongitude;
    private BigDecimal weightedAverageLatitude;
}
