package ee.bcs.valiit.project.data;

import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

@Transactional
public interface CompanyRepository extends JpaRepository<Company, Long> {

    Company findByName(String name);

}
